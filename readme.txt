====
Misc
====
TODO: factor out fetch promises
TODO: consider imperative redirect
(see https://tylermcginnis.com/react-router-programmatically-navigate/)
TODO: refactor to presentation/container paradigm
(see https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0)

=====
Goals
=====
1. Capture product details from Aliexpress to local db (via browser extension/bookmarklet)
2. Input fields (EnrichedProduct):
 - Title
 - Description
 - Meta desc
 - SEO link
 - Ad copy
 - Optional: Variants, image selection
3. Export to:
 - Facebook ads (either output a csv or via API)
 - Optional: Oberlo/Shopify (Not essential)

=============
API Reference
=============
Oberlo API: https://jsapi.apiary.io/apis/oberlosupplysupplier/reference/products/product/update-product.html
Shopify API: https://help.shopify.com/en/api/reference/products/product#create
Facebook Python SDK: https://developers.facebook.com/docs/marketing-api/reference/sdks/python/campaign/v3.2
Facebook REST API: https://developers.facebook.com/docs/marketing-api/buying-api#campaign

==========
Components
==========
ProductPage
  ProductNav
  ProductTable
    ProductRow
      ProductActions
ProductEdit
  ProductFormAttribute

==========
Mini-goals
==========
Backend: Rework to use safrs or something that autogens RESTfulness from db model (in progress)
Backend/frontend: Create entry point for adding URLs to scrape (in progress)
Scraping: Feed scrapy spider with a list of URLs from database (in progress)

Scraping: import config from home dir for crawling API key
Scraping: Fix json format for multiple products
Scraping: Set up scraped image file structure to public/images/[PRODUCT_SLUG]/
Frontend: Handle multiple images
Backend: Reconsider product model (captured vs. editable fields)
Backend/frontend: Set immutable fields (e.g. id)
Backend/frontend: Set enum field for category
Frontend: "Delete" functionality
Scraping: Write scrapy spider with response.css selectors and image_urls populated

====
Done
====
Frontend: Be able to edit products

==============
Rafik concerns
==============
internet - searching for product - 30
	does it have epack and how much is it
	how many orders
	price
	name
	margins (our cost, sell price, profit(based on default vaule), video)
	go to oberlo button with url in clipboard

oberlo 	 - importing product	 - 15-20
	requires time


shopify  - fleshing out product	 - 30
	order pictueres
	description
	title
	varients
	seo text

video/PS - editing 				 - 30-45
	requires time

facebook - creating add			 - 15-20
	templating
	we can have many templates (WELL NAMED) that we can use for a variety of different ads (this will also take time)
	publish ad

=====
Other
=====
Git repo: Create separate repo (submodule) for volatile assets
public/images
products.json
products.db
alicrawler/.scrapy
config.py
        <div>
            <ul>
            	<li style="list-style: disc; margin-left:15px"><b>Free shipping</b></li>
                <li style="list-style: disc; margin-left:15px"><b><a style="text-decoration:underline" href="https://glowurban.com/pages/return-policy">Return policy</a>: Full refund up to one year after purchase</b></li>
            	<li style="list-style: disc; margin-left:15px"><b>Estimated shipping time: 20 to 30 days</b></li>
            </ul>
        </div> 
def _substitute(self, template, vars):
    def flatten_nested(node):
        flattened = {}

        for key, value in node.items():
            new_value = value

            # We need to go deeper.
            if isinstance(value, dict):
                print("Flattening {}".format(key))
                new_value = flatten_nested(value)
                flattened[key] = new_value
            else:
                # It's a value (leaf node), process substitution and return it.
                # Template variable substitution can only happen with strings.
                if is_substitutable(value):
                    new_value = substitute_var(value, vars)
                flattened[key] = new_value

        return flattened

    return flatten_nested(template)

def flatten_nested(node, key=None):
    flattened = {}

    if isinstance(node, list):
        new_list = []

        for item in node:
            new_item = flatten_nested(node)
            new_list.append(new_item)
            if key:
                flattened[key] = new_list
            else:
                if is_substitutable(value):
                    new_value = substitute_var(value, vars)

    else:
        for key, value in node.items():
            new_value = value

            # We need to go deeper.
            if isinstance(value, dict):
                print("Flattening {}".format(key))
                new_value = flatten_nested(value, key)
                flattened[key] = new_value
            else:
                # It's a value (leaf node), process substitution and return it.
                # Template variable substitution can only happen with strings.
                if is_substitutable(value):
                    new_value = substitute_var(value, vars)
                flattened[key] = new_value

    return flattened

return flatten_nested(template)

