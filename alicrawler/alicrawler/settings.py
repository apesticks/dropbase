# https://doc.scrapy.org/en/latest/topics/settings.html
# https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#https://doc.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'alicrawler'
SPIDER_MODULES = ['alicrawler.spiders']
NEWSPIDER_MODULE = 'alicrawler.spiders'
FEED_FORMAT = "json"
FEED_URI = "../volatile/products.json"

ROBOTSTXT_OBEY = False
TELNETCONSOLE_ENABLED = False
HTTPCACHE_ENABLED = True
HTTPCACHE_EXPIRATION_SECS = 0
HTTPCACHE_DIR = 'httpcache'
HTTPCACHE_IGNORE_HTTP_CODES = []
HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'
ITEM_PIPELINES = {
  'scrapy.pipelines.images.ImagesPipeline': 1
}
IMAGES_STORE = '../public/images'

EXTENSIONS = {
    'scrapy.extensions.telnet.TelnetConsole': None,
}

SPIDER_MIDDLEWARES = {
  'alicrawler.middlewares.AlicrawlerSpiderMiddleware': 543,
}
DOWNLOADER_MIDDLEWARES = {
  'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
  'scrapy_useragents.downloadermiddlewares.useragents.UserAgentsMiddleware': 500,
}

USER_AGENTS = [
  ('Mozilla/5.0 (X11; Linux x86_64) '
   'AppleWebKit/537.36 (KHTML, like Gecko) '
   'Chrome/57.0.2987.110 '
   'Safari/537.36'),  # chrome
  ('Mozilla/5.0 (X11; Linux x86_64) '
   'AppleWebKit/537.36 (KHTML, like Gecko) '
   'Chrome/61.0.3163.79 '
   'Safari/537.36'),  # chrome
  ('Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) '
   'Gecko/20100101 '
   'Firefox/55.0'),  # firefox
  ('Mozilla/5.0 (X11; Linux x86_64) '
   'AppleWebKit/537.36 (KHTML, like Gecko) '
   'Chrome/61.0.3163.91 '
   'Safari/537.36'),  # chrome
  ('Mozilla/5.0 (X11; Linux x86_64) '
   'AppleWebKit/537.36 (KHTML, like Gecko) '
   'Chrome/62.0.3202.89 '
   'Safari/537.36'),  # chrome
  ('Mozilla/5.0 (X11; Linux x86_64) '
   'AppleWebKit/537.36 (KHTML, like Gecko) '
   'Chrome/63.0.3239.108 '
   'Safari/537.36'),  # chrome
]

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://doc.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
#DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Enable or disable downloader middlewares
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#DOWNLOADER_MIDDLEWARES = {
#    'alicrawler.middlewares.AlicrawlerDownloaderMiddleware': 543,
#}
# Enable or disable extensions
# See https://doc.scrapy.org/en/latest/topics/extensions.html

# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
#ITEM_PIPELINES = {
#    'alicrawler.pipelines.AlicrawlerPipeline': 300,
#}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
#AUTOTHROTTLE_START_DELAY = 1
#AUTOTHROTTLE_MAX_DELAY = 30
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
#AUTOTHROTTLE_DEBUG = True
