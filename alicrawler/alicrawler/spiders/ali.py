# -*- coding: utf-8 -*-
import scrapy
from proxycrawl.proxycrawl_api import ProxyCrawlAPI
#api = ProxyCrawlAPI({ 'token': 'YOUR_TOKEN' })

class AliSpider(scrapy.Spider):
  name = 'ali'
  allowed_domains = ['microsoft.com']
  start_urls = [
    'https://microsoft.com'
  ]
  #allowed_domains = ['aliexpress.com']
  #start_urls = ['http://aliexpress.com/']
  #start_urls = list(map(lambda url: api.buildURL(url, {}), urls))

  def parse(self, response):
    an_img = response.css("img::attr(src)").extract()[2]
    yield {
      "header": response.css("div .c-heading::text").extract_first(),
      "image_urls": [
        an_img,
      ]
    }
