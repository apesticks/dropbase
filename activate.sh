#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [[ $(command -v pacman) && ! -f /usr/bin/wget ]]; then
    sudo pacman -Suy --noconfirm wget tar nettle
    sudo pacman -Suy --noconfirm
fi

if [[ -d "$SCRIPT_DIR/.venv" ]]; then
    source $SCRIPT_DIR/.venv/bin/activate
else
    virtualenv .venv
    source $SCRIPT_DIR/.venv/bin/activate
    npm run bootstrap
fi

PATH=$(realpath $SCRIPT_DIR/bin):$PATH
SITE_PACKAGES=$(realpath $(find ./.venv -type d | grep site-packages$))

# Create a symlink to the module in the project path in our venv's site-packages
#ln -s adutil $SITE_PACKAGES/adutil &>/dev/null
# NOTE: The rm is needed, otherwise the ln command will interpret the destination as a path rather than the location where the resulting symlink should go.
rm $SITE_PACKAGES/dropbase && ln -s $SCRIPT_DIR/dropbase $SITE_PACKAGES/dropbase &>/dev/null

if [[ ! -d $SITE_PACKAGES/facebook_business-4.0.0-py3.7.egg ]]; then
    if [[ ! -f /usr/bin/wget ]]; then
        echo "ERROR: You must install wget before proceeding"
    fi

    wget https://github.com/facebook/facebook-python-business-sdk/archive/4.0.0.tar.gz -O /tmp/fb.tar.gz
    cd /tmp
    tar zxvf /tmp/fb.tar.gz
    cd facebook-python-business-sdk-4.0.0
    python setup.py install
fi
