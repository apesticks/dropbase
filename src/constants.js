export const HEADERS_PRODUCTS = [
  'Name',
  'Image',
  '# Orders',
  'Our Price',
  'Ship Cost',
  'Actions',
];
export const HEADERS_SCRAPING = [
  'URL',
  'Last crawled',
  '# Images',
  '# Videos',
  'Product',
];
export const CATEGORIES = [
  'live',
  'research',
  'shelved',
];
export const FIELD_TYPES = {
  category: 'form-control',
  cost: 'form-control',
  id: 'form-control-plaintext',
  img_url: 'form-control',
  name: 'form-control',
  num_orders: 'form-control',
  our_price: 'form-control',
  ship_cost: 'form-control',
  url: 'form-control',
};
export const FIELD_MUTABILITY = {
  category: '',
  cost: '',
  id: 'readonly',
  img_url: '',
  name: '',
  num_orders: '',
  our_price: '',
  ship_cost: '',
  url: '',
};

