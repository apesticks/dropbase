import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css'

import './index.css';
import { ProductPage } from './logic.js';

ReactDOM.render(
  <BrowserRouter>
    <ProductPage />
  </BrowserRouter>,
  document.getElementById('root')
);
