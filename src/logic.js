import React from 'react';
import { Component } from 'react';
import { Redirect, Route } from 'react-router-dom';

import {
  CATEGORIES,
  FIELD_MUTABILITY,
  FIELD_TYPES,
  HEADERS_PRODUCTS,
} from './constants.js';
import {
  ProductAdd,
  ProductNav,
  ProductRow,
  ProductScraping,
  ProductScrapingRow,
} from './presentation.js';

class ProductEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: {},
      redirect: false
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFieldChange = this.handleFieldChange.bind(this);
  }

  componentDidMount() {
    const { id } = this.props.match.params;

    fetch(`http://localhost:3000/products/${id}`)
      .then(res => res.json())
      .then(json => {
        if (json.product.data !== undefined) {
          this.setState({product: json.product.data, redirect: false});
        }
    });
  }

  handleSubmit(event) {
    const { product } = this.state;
    fetch(`http://localhost:3000/products/${product.id}`,
      {
        method: 'POST',
        body: JSON.stringify(product),
        headers: {"Content-Type": "application/json"},
      }
    )
    .then(res => res.json())
    .then(json => {
      this.setState({product: product, redirect: true});
    })
    .catch(error => console.error(error));

    event.preventDefault();
  }

  handleFieldChange(fieldId, value) {
    const { product } = this.state;
    const new_product = {...product, [fieldId]: value}

    this.setState({product: new_product, redirect: false});
  }

  render() {
    const { redirect, product } = this.state;
    const { category } = this.state.product;

    if (redirect) {
      return <Redirect to={`/${category}`} />
    }

    return (
      <div className='page product-edit'>
        <h3>Editing product {product.name} ({product.id})</h3>
				<form onSubmit={this.handleSubmit}>
					{ Object.entries(product).map(entry => (
						<ProductFormAttribute
							key={'pa_' + product.id + '_' + entry[0]}
							field={entry[0]}
							value={this.state.product[entry[0]]}
							onChange={this.handleFieldChange}
							type={FIELD_TYPES[entry[0]]}
							readOnly={FIELD_MUTABILITY[entry[0]]}
						/>
					))}
          <button type="submit" className="btn btn-primary">Submit</button>
				</form>
      </div>
    )
  }
}

class ProductFormAttribute extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    // pass applicable field changes to callback from props
    const value = event.target.value;
    this.props.onChange(this.props.field, value);
  }

  render() {
    const { field, value, readOnly, type } = this.props;
    return (
			<div className="form-group row">
				<label htmlFor={field} className="col-sm-2 col-form-label">{field}</label>
				<div className="col-sm-10">
					<input
						type="text"
						readOnly={readOnly}
            className={type}
            id={field}
            defaultValue={value}
            onChange={this.handleChange}
          />
				</div>
			</div>
    )
  }
}

export class ProductScrapingTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      settings: [],
      isLoaded: false,
    };
  }

  componentDidMount() {
    /*
    fetch(`http://localhost:3000/scraping`)
      .then(res => res.json())
      .then(json => {
        if (json.data !== undefined) {
          this.setState({
            settings: json.data,
            isLoaded: true,
          })
        }
    });
    */
  }

  render() {
    const { headers } = this.props;
    const { settings } = this.state;

    return (
      <table className='table'>
        <thead>
          <tr>
            { headers.map(header => (
              <th key={header}>{header}</th>
            ))}
          </tr>
        </thead>
        <tbody>
        { settings.map(setting => (
          <ProductScrapingRow key={setting.id} setting={setting} />
        ))}
        </tbody>
      </table>
    )
  }
}

class ProductTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      isLoaded: false,
    };
  }

  componentDidMount() {
    const { category } = this.props;
    fetch(`http://localhost:3000/products?category=${category}`)
      .then(res => res.json())
      .then(json => {
        if (json.data !== undefined) {
          this.setState({
            products: json.data,
            isLoaded: true,
          })
        }
    });
  }

  render() {
    const { headers } = this.props;
    const { products } = this.state;

    return (
      <table className='table'>
        <thead>
          <tr>
            { headers.map(header => (
              <th key={header}>{header}</th>
            ))}
          </tr>
        </thead>
        <tbody>
        { products.map(product => (
          <ProductRow key={product.name} product={product} />
        ))}
        </tbody>
      </table>
    )
  }
}

export class ProductPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false
    };
  }

  componentDidMount() {
    this.setState({isLoaded: true});
  }

  renderTable(products) {
    return (
      <div className='page product-page'>
        { CATEGORIES.map(category => (
            <Route key={category} path={'/' + category} render={
              (props) =>
                <ProductTable
                  headers={HEADERS_PRODUCTS}
                  category={category}
                />
              }
            />
          ))
        }
      </div>
    )
  }

  assignRoutes(products) {
    return (
      <div>
        <Route key='productroute' path='/edit/:id'
          render={
            (props) => <ProductEdit {...props} />
          }
        />
        <Route key='addproduct' path='/add'
          render={
            (props) => <ProductAdd {...props} />
          }
        />
        <Route key='scraping' path='/scraping'
          render={
            (props) => <ProductScraping {...props} />
          }
        />
      </div>
    )
  }

  loadTable() {
    var { isLoaded } = this.state;
    let output;

    output = (
      <div>
        { this.renderTable() }
        { this.assignRoutes() }
      </div>
    );

    if (!isLoaded) {
      output = <div>Loading page...</div>
    }

    return output;

  }

  render() {
    return (
      <div className='product-app'>
        <ProductNav className='product-nav' />
        <div className='product-page'>
          { this.loadTable() }
        </div>
      </div>
    );
  }
}
