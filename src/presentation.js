import React from 'react';
import { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';

// Font-awesome icons
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTimes } from '@fortawesome/free-solid-svg-icons';

import {
  HEADERS_SCRAPING,
} from './constants.js';
import { ProductScrapingTable } from './logic.js';

library.add(faEdit)
library.add(faTimes)

export class ProductAdd extends Component {
  render() {
    return (
      <div className='page product-add'>
				<form>
					<div className="form-group">
            <h3><label htmlFor="urls">URLs (one per line)</label></h3>
						<textarea className="form-control" id="urls" rows="10"></textarea>
					</div>
					<button type="submit" className="btn btn-primary">Submit</button>
				</form>
      </div>
    )
  }
}

export class ProductScraping extends Component {
  render() {
    return (
      <div className='page product-scraping'>
        <ProductScrapingTable headers={HEADERS_SCRAPING} />
      </div>
    )
  }
}


export class ProductScrapingRow extends Component {
  render() {
    const { setting } = this.props;

    return (
      <tr key={'setting' + setting.id}>
        <td><a href={setting.url}>{setting.url}</a></td>
      </tr>
    )
  }
}

export class ProductActions extends Component {
  render() {
    const { id } = this.props;

    return (
      <div className='actions'>
        <Link to={'/edit/' + id}><FontAwesomeIcon icon='edit' /></Link>
        <Link to={'/delete/' + id}><FontAwesomeIcon icon='times' /></Link>
      </div>
    )
  }
}

export class ProductRow extends Component {
  render() {
    const { product } = this.props;

    return (
      <tr key={'product' + product.id}>
        <td><a href={product.url}>{product.name}</a></td>
        <td><img src={product.img_ur} alt={product.name}/></td>
        <td>{product.num_orders}</td>
        <td>{product.our_price}</td>
        <td>{product.ship_cost}</td>
        <td><ProductActions id={product.id} /></td>
      </tr>
    )
  }
}

export class ProductNav extends Component {
  render() {
    return (
      <ul className='nav nav-tabs'>
        <li className='nav-item'>
          <NavLink className='nav-link' to='/live'>Live</NavLink>
        </li>
        <li className='nav-item'>
          <NavLink className='nav-link' to='/research'>Research</NavLink>
        </li>
        <li className='nav-item'>
          <NavLink className='nav-link' to='/shelved'>Shelved</NavLink>
        </li>
        <li className='nav-item'>
          <NavLink className='nav-link' to='/scraping'>Scraping</NavLink>
        </li>
        <li className='nav-item'>
          <NavLink className='nav-link' to='/add'>Add products</NavLink>
        </li>
      </ul>
    )
  }
}
