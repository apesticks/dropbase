from collections import Mapping
from pathlib2 import PurePath
import six
import pdb

# This code was imported from https://github.com/ianlini/flatten-dict
# (with slight modification to allow for list reconstruction)


def tuple_splitter(flat_key):
    return flat_key


def path_splitter(flat_key):
    keys = PurePath(flat_key).parts
    return keys


def dot_splitter(flat_key):
    return flat_key.split(".")


def tuple_reducer(k1, k2):
    if k1 is None:
        return (k2,)
    else:
        return k1 + (k2,)


def path_reducer(k1, k2):
    import os.path

    if k1 is None:
        return k2
    else:
        return os.path.join(k1, k2)


def dot_reducer(k1, k2):
    if not k1:
        return k2
    return "{}.{}".format(k1, k2)


REDUCER_DICT = {"tuple": tuple_reducer, "path": path_reducer, "dot": dot_reducer}

SPLITTER_DICT = {"tuple": tuple_splitter, "path": path_splitter, "dot": dot_splitter}


def flatten(d, reducer="tuple", inverse=False, enumerate_types=()):
    """Flatten `Mapping` object.

    Parameters
    ----------
    d : dict-like object
        The dict that will be flattened.
    reducer : {'tuple', 'path', Callable}
        The key joining method. If a `Callable` is given, the `Callable` will be
        used to reduce.
        'tuple': The resulting key will be tuple of the original keys.
        'path': Use `os.path.join` to join keys.
    inverse : bool
        Whether you want invert the resulting key and value.
    enumerate_types : Sequence[type]
        Flatten these types using `enumerate`.
        For example, if we set `enumerate_types` to ``(list,)``,
        `list` indices become keys: ``{'a': ['b', 'c']}`` -> ``{('a', 0): 'b', ('a', 1): 'c'}``.

    Returns
    -------
    flat_dict : dict
    """
    enumerate_types = tuple(enumerate_types)
    flattenable_types = (Mapping,) + enumerate_types
    if not isinstance(d, flattenable_types):
        raise ValueError(
            "argument type %s is not in the flattenalbe types %s"
            % (type(d), flattenable_types)
        )

    if isinstance(reducer, str):
        reducer = REDUCER_DICT[reducer]
    flat_dict = {}

    def _flatten(d, parent=None):
        key_value_iterable = (
            enumerate(d) if isinstance(d, enumerate_types) else six.viewitems(d)
        )
        for key, value in key_value_iterable:
            flat_key = reducer(parent, key)
            if isinstance(value, flattenable_types):
                _flatten(value, flat_key)
            else:
                if inverse:
                    flat_key, value = value, flat_key
                if flat_key in flat_dict:
                    raise ValueError("duplicated key '{}'".format(flat_key))
                flat_dict[flat_key] = value

    _flatten(d)
    return flat_dict


def nested_set_dict(d, keys, value):
    """Set a value to a sequence of nested keys

    Parameters
    ----------
    d : Mapping
    keys : Sequence[str]
    value : Any
    """
    assert keys
    key = keys[0]
    if len(keys) == 1:
        if key in d:
            raise ValueError("duplicated key '{}'".format(key))
        d[key] = value
        return
    d = d.setdefault(key, {})
    nested_set_dict(d, keys[1:], value)


def reassemble(d, was_list=False):
    """Recursively transform dictionaries with integer keys into lists"""
    if not was_list:
        was_list = {"status": False}

    if isinstance(d, Mapping):
        to_replace = {}
        for key, value in six.viewitems(d):
            new_value = reassemble(value, was_list)

            if was_list["status"] == True:
                to_replace[key] = new_value

        for key, value in to_replace.items():
            d[key] = value

        all_keys = [item[0] for item in d.keys()]
        keys_are_ints = all([key.isdigit() for key in all_keys])
        if keys_are_ints:
            was_list["status"] = True
            return list(d.values())

    return d


def unflatten(d, splitter="tuple", inverse=False):
    """Unflatten dict-like object.

    Parameters
    ----------
    d : dict-like object
        The dict that will be unflattened.
    splitter : {'tuple', 'path', Callable}
        The key splitting method. If a Callable is given, the Callable will be
        used to split.
        'tuple': Use each element in the tuple key as the key of the unflattened dict.
        'path': Use `pathlib.Path.parts` to split keys.
    inverse : bool
        Whether you want to invert the key and value before flattening.

    Returns
    -------
    unflattened : dict or list
    """
    if isinstance(splitter, str):
        splitter = SPLITTER_DICT[splitter]

    unflattened = {}

    try:
        all_keys = [item[0] for item in d.keys()]
        keys_are_single_length = [tuple([key]) for key in all_keys] == list(d.keys())
        keys_are_consecutive = sorted(all_keys) == list(
            range(min(all_keys), max(all_keys) + 1)
        )
        keys_are_ints = all([isinstance(key, int) for key in all_keys])

        if keys_are_single_length and keys_are_ints and keys_are_consecutive:
            return list(d.values())
    except TypeError:
        for flat_key, value in six.viewitems(d):
            if inverse:
                flat_key, value = value, flat_key
            key_tuple = splitter(flat_key)
            nested_set_dict(unflattened, key_tuple, value)

        unflattened = reassemble(unflattened)

        return unflattened
