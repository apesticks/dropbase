import pprint
import yaml
from dropbase.ads.api import FacebookApi
from dropbase.ads.models import DotKeyed, Template
import os
import pdb
import json
import yaml
import copy
from dropbase import config

REPO_PATH = os.path.dirname(
    os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
)


def pprint_dict(d):
    """ Pretty print a dict better than stdlib. """
    print(json.dumps(d, indent=4, separators=(",", ":")))


def resolve_template_path(filename):
    project_root = os.path.dirname(os.path.dirname(__file__))
    resolved_filename = os.path.join(project_root, filename)
    return resolved_filename


def get_campaigns():
    fb = FacebookApi()
    campaigns = fb.get_campaigns()
    for campaign in campaigns:
        yield campaign


def confirm_delete(campaign, force=False):
    user_input = "n"
    name = campaign.summary().get('name', '')

    if not force:
        user_input = input("Are you sure you want to delete {} ({}) [n]? ".format(campaign, name))

    if user_input == "y" or force:
        campaign.delete()


def substitute_var(value, new_items):
    # Default to existing value, if key is not specified for substitution.
    new_value = value or ""

    key = value.strip("<>")
    new_value = new_items.get(key, value)
    return new_value


def is_substitutable(o):
    """ Is it a string wrapped in angle brackets? """
    if isinstance(o, str):
        if o.startswith("<") and o.endswith(">"):
            return True


def split_campaign_objects(parsed_parent):
    parsed = copy.deepcopy(parsed_parent)

    adsets = parsed.pop("adsets")
    return parsed, adsets


def is_float(o):
    try:
        float(o)
        return True
    except ValueError:
        return False


def coerce_to_native_type(o):
    if o.isdigit():
        return int(o)
    if is_float(o):
        return float(o)
    return o


class TemplateParser(object):
    def __init__(self, filename, **template_vars):
        self._parent_filename = None

        self._output = self._parse(filename, **template_vars)

    @property
    def parent_filename(self):
        return self._parent_filename

    @property
    def parent_filename_path(self):
        parent_filename = os.path.join(
            REPO_PATH, config.TEMPLATE_DIR, self.parent_filename
        )
        parent_filename_full = resolve_template_path(parent_filename)
        return parent_filename_full

    @property
    def output(self):
        return self._output

    @staticmethod
    def _find_parent_template(tree):
        """ Find a special key containing a parent template filename. """
        for key, value in tree.items():
            if key == "_parent":
                return value

    def _strip_metavars(self, tree):
        """ Strip any special keys from a template (prefixed in _). They
        should not be exposed to the parent template. """
        # NOTE: Only keys at the top level of the template are considered
        to_delete = []

        # Before clearing it out, set parent templater.
        self._parent_filename = self._find_parent_template(tree)

        for key in list(tree.keys()):
            if key[0] == "_":
                del tree[key]

        return tree

    def _augment_template_vars(self):
        # TODO: Hook up machinery or remove this function
        special_vars = {}
        return special_vars

    def _substitute_vars(self, template, vars):
        # Iterate over each value which *could* be shoved into the template
        for var, new_value in vars.items():
            for key, var_to_replace in template.vars_by_key.items():
                # If there's any hope that this thing will be replaced, it will
                # look like this
                pre_substituted = "<{}>".format(var)

                if pre_substituted in var_to_replace:
                    try:
                        updated_value = var_to_replace.replace(
                            pre_substituted, str(new_value)
                        )
                        # updated_value = coerce_to_native_type(updated_value)
                    except Exception as e:
                        print(e)
                        pdb.set_trace()

                    print("Updating {} to {}".format(key, updated_value))
                    template.deep_update(key, updated_value)

        # pprint_dict(template)
        # pdb.set_trace()

        return template

    def _parse_template_file(self, ffo):
        raw = yaml.safe_load(ffo)
        template = Template(raw)
        return template

    def _parse(self, filename, **vars):
        """ Parse a template yaml file into a dictionary, extracting parent
        template filename, if applicable.

        If template_vars are provided, they will be substituted into the
        template in the place of variables of the same names enclosed within
        angle brackets.
        """
        # Template vars prefixed in "this." have special meaning. These must
        # be supplied ahead of time to the substitution vars
        vars.update(self._augment_template_vars())

        with open(filename, "r") as f:
            # 1. Convert the yaml to a dict
            # 2. Strip any special keys (prefixed in an underscore)
            # 3. Substitute template vars (enclosed in angle brackets)
            template = self._parse_template_file(f)
            sanitized_template = self._strip_metavars(template)

            # If there are any variables to subtitute, go ahead and do so
            if vars:
                sanitized_template = self._substitute_vars(sanitized_template, vars)

            return sanitized_template
