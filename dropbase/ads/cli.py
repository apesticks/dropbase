#!/usr/bin/env python
import os
import sys
import pdb

from argparse import ArgumentParser
from dropbase.ads.models import Campaign, Adset
from dropbase.ads.utils import (
    TemplateParser,
    get_campaigns,
    confirm_delete,
    split_campaign_objects,
    pprint_dict,
)


def parse_args():
    parser = ArgumentParser(prog="dropbase ads")
    parser.add_argument(
        "-t",
        "--template",
        help="Create an ad campaign based on a yaml template",
    )
    #parser.add_argument("-b", "--parenttemplate", help="Select a parent template")
    parser.add_argument(
        "-l", "--list", help="List published campaigns", action="store_true"
    )
    parser.add_argument(
        "-n", "--name", help="Specify name of campaign to be updated"
    )
    parser.add_argument("-d", "--delete", help="Delete campaigns", action="store_true")
    parser.add_argument(
        "-f",
        "--force",
        help="Force action that would normally require confirmation",
        action="store_true",
    )
    args = parser.parse_args()

    if len(sys.argv) <= 1:
        parser.print_help()

    return args


def main():
    args = parse_args()

    if args.template:
        parser = TemplateParser(args.template)

        # Step one: Extract campaign data from a specified yaml
        # template (and its parent template)
        parsed_parent = TemplateParser(
            parser.parent_filename_path, **parser.output
        ).output
        parsed_campaign, parsed_adsets = split_campaign_objects(parsed_parent)

        # Step two: Create the campaign via the Facebook API
        staged_campaign = Campaign(**parsed_campaign)
        campaign = staged_campaign.apply()

        # Step three: Create the adsets and assign them to the campaign
        staged_adsets = [
            Adset(campaign_id=campaign.id, **parsed_adset)
            for parsed_adset in parsed_adsets
        ]
        for adset in staged_adsets:
            adset = adset.apply()

        print("Successfully created campaign: {}".format(campaign.id))

    if args.list or args.delete:
        campaigns = get_campaigns()
        for campaign in campaigns:
            summary = campaign.summary()

            if args.list:
                print(summary)

            if args.delete:
                if args.name:
                    if args.name.lower() in summary.get('name', '').lower():
                        confirm_delete(campaign, force=args.force)
                else:
                    # TODO: Add way to delete all?
                    raise Exception("You must specify a campaign name to delete (-n option).")


if __name__ == "__main__":
    sys.exit(main())
