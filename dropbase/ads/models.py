# from utils import get_api_action_name
from dropbase.ads.api import FacebookApi
from dropbase.ads.flatten import flatten, unflatten
import pdb
import json


def pprint_dict(d):
    """ Pretty print a dict better than stdlib. """
    print(json.dumps(d, indent=4, separators=(",", ":")))


def get_api_action_name(instance):
    """ Look at FacebookApi._api attributes for options """
    if instance.__class__.__name__ == "Campaign":
        return "create_campaign"

    if instance.__class__.__name__ == "Adset":
        return "create_ad_set"


def dot_flatten(target):
    output = flatten(target, reducer="dot", enumerate_types=(list,))
    return output


def dot_unflatten(target):
    return unflatten(target, splitter="dot")


def pprint_dict(d):
    """ Pretty print a dict better than stdlib. """
    print(json.dumps(d, indent=4, separators=(",", ":")))


class DotKeyed(dict):
    def __getattr__(self, key):
        return self.get(key)


class FacebookObject(DotKeyed):
    required_fields = []

    def __init__(self, **params):
        self.status = "staged"
        self._params = params
        pprint_dict(self._params)

        self.update(params)
        self._missing_field_check()

    @property
    def class_name(self):
        return self.__class__.__name__

    def _missing_field_check(self):
        for field in self.required_fields:
            if field not in self:
                raise Exception(
                    "Missing field for {}: {}".format(self.class_name, field)
                )

    def get_api_action(self):
        fb = FacebookApi()
        action_name = get_api_action_name(self)
        action = getattr(fb, action_name)
        return action

    def apply(self):
        api_action = self.get_api_action()
        fb_object = api_action(**self)
        self.status = "applied"

        return fb_object

    def __repr__(self):
        return "<{} {} ({})>".format(self.class_name, id(self), self.status)


class Template(dict):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self._parsed = self._process(raw)

    def deep_update(self, key, value):
        flat = dot_flatten(self)
        flat[key] = value
        new = dot_unflatten(flat)

        self.update(new)

    @property
    def deep_keys(self):
        flat = dot_flatten(self)
        return flat.keys()

    @property
    def deep_items(self):
        flat = dot_flatten(self)
        return flat.items()

    @property
    def vars_by_key(self):
        output = {}
        for key, value in self.deep_items:
            if isinstance(value, str) and "<" in value and ">" in value:
                output[key] = value

        return output


class Campaign(FacebookObject):
    required_fields = ["name", "status", "objective"]


class Adset(FacebookObject):
    required_fields = ["name", "status"]


if __name__ == "__main__":
    camp = Campaign(name="wee", status="PAUSED", objective="CONVERSIONS")
    test = camp.apply()
    pdb.set_trace()
