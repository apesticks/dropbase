from facebook_business.api import FacebookAdsApi
from facebook_business.adobjects.adaccount import AdAccount
from dropbase.config import FB_APP_ID, FB_APP_SECRET, FB_ACCESS_TOKEN, DEFAULT_CAMPAIGN
import pdb
import pprint


def pdir(obj):
    pprint.pprint([item for item in dir(obj) if "__" not in item])


def create_wrapped_api_object(obj):
    obj_name = obj.__class__.__name__
    if obj_name == "Campaign":
        obj = FacebookApiCampaign(obj)
    if obj_name == "Adset":
        obj = FacebookApiCampaign(obj)
    if obj_name == "Cursor":
        obj = FacebookApiCursor(obj)
    return obj


class FacebookApiCampaign(object):
    standard_fields = ["id", "name", "objective", "status", "budget_remaining"]

    def __init__(self, orig=None):
        self._raw = orig

    def __getattr__(self, key):
        return self._raw.get(key) or getattr(self._raw, key)

    def __repr__(self):
        return "<{} {}>".format(self.__class__.__name__, self.id)

    def _save(self):
        self.api_update()
        # self.remote_update()

    def delete(self):
        self.api_delete()

    def dir(self):
        pdir(self._raw)

    def summary(self):
        self._raw.api_get(fields=self.standard_fields)
        # A bit yuck, but whatever
        return self._raw._data


class FacebookApiCursor(list):
    def __init__(self, orig):
        self._raw = orig
        self._wrap_all()

    def _wrap_all(self):
        for item in self._raw:
            new = create_wrapped_api_object(item)
            self.append(new)

    def dir(self):
        pdir(self._raw)


class FacebookApi(object):
    def __init__(self):
        self._api = self._generate_api()
        self._api_call_key = None

    def __getattr__(self, key, fields=None):
        try:
            wrapped = getattr(self._api, key)
        except Exception as e:
            raise Exception(
                "Unrecognized api method. Check self._api's attributes for options"
            )
        if callable(wrapped):
            self._api_call_key = key
            return self._api_call

        return self._api.get(key)

    def _api_call(self, *args, **kwargs):
        api_call_inputs = {}
        api_call_inputs.setdefault("fields", [])
        api_call_inputs.setdefault("params", kwargs)

        if self._api_call_key:
            wrapped = getattr(self._api, self._api_call_key)
            output = wrapped(*args, **api_call_inputs)
            return create_wrapped_api_object(output)

    def _generate_api(self):
        FacebookAdsApi.init(FB_APP_ID, FB_APP_SECRET, FB_ACCESS_TOKEN)
        api = AdAccount("act_{}".format(FB_APP_ID))
        return api

    def apropos(self, key):
        return [item for item in dir(self._api) if key in item]


if __name__ == "__main__":
    fb = FacebookApi()
    # params = {"name": "My campaign", "objective": "LINK_CLICKS", "status": "PAUSED"}
    # cc = fb.create_campaign(**params)
    camps = fb.get_campaigns()

    pdb.set_trace()
