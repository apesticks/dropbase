import simplejson as json


class AdTemplate(object):
    def __init__(self, name, **kwargs):
        self.name = name
        self._template = self.parse(kwargs)

    def __getattr__(self, key):
        return self._template.get(key)

    def parse(self, params):
        template = {}
        template["name"] = self._name
        template.update(params)

        return template

    def summary(self):
        return self._template
