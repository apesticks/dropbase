from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow.sqla import ModelSchema
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
import marshmallow_sqlalchemy  # needed for pipreqs :(
import simplejson

from sqlalchemy.exc import IntegrityError
import pipreqs
import pdb

# from marshmallow import Schema, fields, ValidationError, pre_load

app = Flask(__name__)
db = SQLAlchemy(app)
marsh = Marshmallow(app)
migrate = Migrate(app, db)

app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///products.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False


def not_blank(data):
    if not data:
        raise ValidationError("Data not provided")


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    category = db.Column(db.String(80))
    url = db.Column(db.String(1024))
    img_url = db.Column(db.String(512))
    num_orders = db.Column(db.Integer)
    cost = db.Column(db.Numeric)
    ship_cost = db.Column(db.Numeric)
    our_price = db.Column(db.Numeric)


class ProductSchema(marsh.ModelSchema):
    class Meta:
        model = Product


product_schema = ProductSchema()


@app.route("/products")
def get_products():
    filters = dict(request.values.items())
    values = request.values

    products = Product.query.filter_by(**filters)
    output = product_schema.dump(products, many=True)
    return jsonify(output)


@app.route("/products/<int:pk>")
def get_product(pk):
    try:
        product = Product.query.get(pk)
    except IntegrityError:
        return jsonify({"products": "Product not found"}), 400
    result = product_schema.dump(product)
    return jsonify({"product": result})


@app.route("/products/<int:pk>", methods=["POST"])
def update_product(pk):
    try:
        product = Product.query.get(pk)
    except IntegrityError:
        return jsonify({"products": "Product not found"}), 400
    result = product_schema.dump(product)

    # Update a dict with the new values
    result.data.update(request.json)
    new_result = product_schema.load(result.data)
    updated_product = new_result.data

    # Update db
    db.session.add(updated_product)
    db.session.commit()

    return jsonify({"product": result})


@app.route("/products/", methods=["POST"])
def new_product():
    json_data = request.get_json()
    if not json_data:
        return jsonify({"message": "No input data provided"}), 400

    product_check = Product.query.filter_by(name=json_data["name"]).first()
    if product_check:
        return jsonify({"message": "Product already exists"}), 400

    try:
        product = product_schema.load(json_data).data
    except ValidationError as err:
        return jsonify(err.messages), 422

    # data = data[0]

    db.session.add(product)
    db.session.commit()
    fetched_product = Product.query.get(product.id)
    result = product_schema.dump(fetched_product).data

    return jsonify({"message": "Created new product.", "product": result})


if __name__ == "__main__":
    db.create_all()
    app.run(debug=True, port=4000)
