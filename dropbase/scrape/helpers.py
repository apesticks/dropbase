import os
import requests
from lxml import html
#from dropbase.scrape.item import Item, item_exists, item_add
from proxycrawl.proxycrawl_api import ProxyCrawlAPI
from dropbase import config


api = ProxyCrawlAPI({'token': config.PROXY_TOKEN})


def scrape_from_file(file):
    with open(file, encoding="utf8") as f:
        line = f.readline()

        while line:
            page = api.get(line)
            tree = html.fromstring(str(page["body"]))

            sanitized_url = line[:-1].split("?")[0]
            _id = sanitized_url.split("?")[0].split("/")[-1].split(".")[0]

            #if not item_exists(_id):
            name = tree.xpath('//div[@class="product-title"]')[0].text
            price_low = (tree.xpath('//span[@class="product-price-value"]')[0].text.split("$")[-1].split(" - ")[0] if "-" in tree.xpath('//span[@class="product-price-value"]')[0].text.split("$")[-1] else tree.xpath('//span[@class="product-price-value"]')[0].text.split("$")[-1])
            price_high = (tree.xpath('//span[@class="product-price-value"]')[0].text.split("$")[-1].split(" - ")[1] if "-" in tree.xpath('//span[@class="product-price-value"]')[0].text.split("$")[-1] else None)
            num_orders = int(tree.xpath('//span[@class="product-reviewer-sold"]')[0].text.split(" ")[0])
            image_list = str([image.getchildren()[0].attrib["src"] for image in tree.xpath('//div[@class="images-view-item"]')])
            video_exists = bool(len(tree.xpath('//div[@class="video-wrap"]')))
            print("url: %s\nID: %s\nname: %s\nprice_low: %s\nprice_high: %s\nnum_orders: %s\nimage_list: %s\nvideo: %s\n" % (sanitized_url, _id, name, price_low, price_high, num_orders, image_list, video_exists))

                #item = Item(
                #    url=sanitized_url,
                #    product_id=_id,
                #    name=name,
                #    price_low=price_low,
                #    price_high=price_high,
                #    num_orders=num_orders,
                #    image_list=image_list,
                #    video_exists=video_exists,
                #)
                #item_add(item)

            line = f.readline()


def scrape_from_url(url):
    body = cache_url(url)
    tree = html.fromstring(body)

    sanitized_url = url[:-1].split("?")[0]
    _id = url.split("?")[0].split("/")[-1].split(".")[0]
    #if not item_exists(_id):
    name = tree.xpath('//div[@class="product-title"]')[0].text
    price_low = (tree.xpath('//span[@class="product-price-value"]')[0].text.split("$")[-1].split(" - ")[0] if "-" in tree.xpath('//span[@class="product-price-value"]')[0].text.split("$")[-1] else tree.xpath('//span[@class="product-price-value"]')[0].text.split("$")[-1])
    price_high = (tree.xpath('//span[@class="product-price-value"]')[0].text.split("$")[-1].split(" - ")[1] if "-" in tree.xpath('//span[@class="product-price-value"]')[0].text.split("$")[-1] else None)
    num_orders = int(tree.xpath('//span[@class="product-reviewer-sold"]')[0].text.split(" ")[0])
    image_list = str([image.getchildren()[0].attrib["src"] for image in tree.xpath('//div[@class="images-view-item"]')])
    video_exists = bool(len(tree.xpath('//div[@class="video-wrap"]')))
    print("url: %s\nID: %s\nname: %s\nprice_low: %s\nprice_high: %s\nnum_orders: %s\nimage_list: %s\nvideo: %s\n" % (sanitized_url, _id, name, price_low, price_high, num_orders, image_list, video_exists))

        #item = Item(
        #    url=sanitized_url,
        #    product_id=_id,
        #    name=name,
        #    price_low=price_low,
        #    price_high=price_high,
        #    num_orders=num_orders,
        #    image_list=image_list,
        #    video_exists=video_exists,
        #)
        #item_add(item)


def cache_url(url):
    path = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))), "volatile", "cache")
    url_id = url[:-1].split("?")[0].split("/")[-1]
    url_path = os.path.join(path, url_id)

    if os.path.exists(url_path):
        with open(url_path, "r", encoding="utf8") as f:
            body = f.read()
    else:
        with open(url_path, "w", encoding="utf8") as f:
            page = api.get(url)
            body = str(page["body"])
            f.write(body)

    return body


# dynamic
# star_rating - float (0.0 to 5.0)
# has_epacket - bool
