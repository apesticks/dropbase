from argparse import ArgumentParser
import os
import sys


def parse_args():
    parser = ArgumentParser(prog="dropbase scrape")
    parser.add_argument("-k", "--link", help="add url or path to file to scrape")
    args = parser.parse_args()

    if len(sys.argv) <= 1:
        parser.print_help()

    return args


def main():
    args = parse_args()

    if args.link:
        # Import is placed here to make the usage text faster
        from dropbase.scrape.helpers import scrape_from_file, scrape_from_url

        if os.path.exists(args.link):
            scrape_from_file(args.link)
        else:
            scrape_from_url(args.link)


if __name__ == "__main__":
    sys.exit(main())
