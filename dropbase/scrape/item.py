from dropbase.web.cli import db


class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(1024))
    product_id = db.Column(db.Integer)
    name = db.Column(db.String(80))
    price_low = db.Column(db.Numeric)
    price_high = db.Column(db.Numeric)
    num_orders = db.Column(db.Integer)
    image_list = db.Column(db.String())
    video_exists = db.Column(db.Boolean)


def item_exists(product_id):
    product_check = Item.query.filter_by(product_id=product_id).first()
    if product_check:
        return True


def item_add(item):
    db.session.add(item)
    db.session.commit()


if __name__ == "__main__":
    db.create_all()
