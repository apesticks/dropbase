#!/bin/bash
sudo pacman --noconfirm -Suy 
sudo pacman --noconfirm -S python python-pip npm
sudo pip install virtualenv

# Create virtualenv
virtualenv .venv 
source .venv/bin/activate
pip install -r requirements.txt
npm install
